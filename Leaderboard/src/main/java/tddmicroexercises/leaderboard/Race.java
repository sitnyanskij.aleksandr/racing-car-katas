package tddmicroexercises.leaderboard;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Race {

    private static final Integer[] POINTS = new Integer[]{25, 18, 15};
    private final String name;
    private final List<Driver> results;
    private final Map<Driver, String> driverNames;

    public Race(String name, Driver... drivers) {
        this.name = name;
        this.results = Arrays.asList(drivers);
        this.driverNames = new HashMap<>();
        toRace();
    }

    @Override
    public String toString() {
        return name;
    }

    private void toRace() {
        results.forEach(driver -> driverNames.put(driver, driver instanceof SelfDrivingCar
                ? createSelfDrivingName(driver)
                : driver.getName()));
    }

    private String createSelfDrivingName(Driver driver) {
        return "Self Driving Car - " +
                driver.getCountry() +
                " (" +
                ((SelfDrivingCar) driver).getAlgorithmVersion() +
                ")";
    }

    public int getDriverPosition(Driver driver) {
        return this.results.indexOf(driver);
    }

    public int getDriverPoints(Driver driver) {
        return Race.POINTS[getDriverPosition(driver)];
    }

    public List<Driver> getResults() {
        return results;
    }

    public String getDriverName(Driver driver) {
        return this.driverNames.get(driver);
    }
}
