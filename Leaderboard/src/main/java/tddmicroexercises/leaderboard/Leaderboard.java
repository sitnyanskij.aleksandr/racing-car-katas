package tddmicroexercises.leaderboard;

import java.util.*;
import java.util.stream.Collectors;

public class Leaderboard {

    private final List<Race> races;

    public Leaderboard(Race... races) {
        this.races = Arrays.asList(races);
    }

    public Map<String, Integer> calculateDriverResults() {
        Map<String, Integer> results = new HashMap<>();
        races.forEach(race -> race.getResults().forEach(driver -> fillResults(results, race, driver)));
        return results;
    }

    private void fillResults(Map<String, Integer> results, Race race, Driver driver) {
        String driverName = race.getDriverName(driver);
        int points = race.getDriverPoints(driver);
        results.put(driverName, results.containsKey(driverName) ? results.get(driverName) + points : points);
    }

    public List<String> getDriverRankings() {
        Map<String, Integer> results = calculateDriverResults();
        return results
                .keySet()
                .stream()
                .sorted(Comparator.comparing(results::get).reversed())
                .collect(Collectors.toList());
    }
}