package tddmicroexercises.leaderboard;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class RaceTest {

    @Test
    void isShouldCalculateDriverPoints() {
        // setup

        // act

        // verify
        assertEquals(25, TestData.race1.getDriverPoints(TestData.driver1));
        assertEquals(18, TestData.race1.getDriverPoints(TestData.driver2));
        assertEquals(15, TestData.race1.getDriverPoints(TestData.driver3));
    }
}