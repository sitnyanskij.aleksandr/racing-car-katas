package tddmicroexercises.textconvertor;

import java.io.IOException;

public abstract class HtmlConverter {

    private final String filePath;

    public HtmlConverter(String filePath) {
        this.filePath = filePath;
    }

    protected String convert() throws IOException {
        return null;
    }

    protected String convert(int page) throws IOException {
        return null;
    }

    protected String getFilePath() {
        return filePath;
    }

    protected String concatLines(String line) {
        return StringEscapeUtils.escapeHtml(line) + "<br />";
    }
}