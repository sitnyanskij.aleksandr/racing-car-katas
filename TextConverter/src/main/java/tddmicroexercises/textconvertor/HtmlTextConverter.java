package tddmicroexercises.textconvertor;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Optional;

public class HtmlTextConverter extends HtmlConverter {

    public HtmlTextConverter(String filePath) {
        super(filePath);
    }

    @Override
    public String convert() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(super.getFilePath()))) {
            Optional<String> convertedString = reader.lines().map(this::concatLines).reduce(String::concat);
            return convertedString.orElse("");
        }
    }
}