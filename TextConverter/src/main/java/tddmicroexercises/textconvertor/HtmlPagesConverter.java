package tddmicroexercises.textconvertor;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

public class HtmlPagesConverter extends HtmlConverter {

    private final List<Integer> breaks;

    public HtmlPagesConverter(String filePath) {
        super(filePath);
        this.breaks = new ArrayList<>();
    }

    @Override
    protected String convert(int page) throws IOException {
        fillBreaks();
        try (BufferedReader reader = new BufferedReader(new FileReader(super.getFilePath()))) {
            Optional<String> reduce = reader
                    .lines()
                    .skip(breaks.get(page))
                    .takeWhile(s -> !s.contains("PAGE_BREAK"))
                    .map(this::concatLines)
                    .reduce(String::concat);
            return reduce.orElse("");
        }
    }

    private void fillBreaks() throws IOException {
        breaks.add(0);
        try (BufferedReader reader = new BufferedReader(new FileReader(super.getFilePath()))) {
            AtomicInteger count = new AtomicInteger();
            reader.lines().forEach(s -> {
                if (s.contains("PAGE_BREAK")) {
                    breaks.add(count.get() + 1);
                    count.set(0);
                }
                count.getAndIncrement();
            });
        }
    }
}