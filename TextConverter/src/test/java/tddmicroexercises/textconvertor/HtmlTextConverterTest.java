package tddmicroexercises.textconvertor;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class HtmlTextConverterTest {

    @Test
    @DisplayName("Method must convert all lines in file into one String")
    void convertToHmlTest() throws IOException {
        HtmlTextConverter converter = new HtmlTextConverter("src/test/resources/TextForHtmlTextConverter.txt");
        String expectedString = "some text &amp; text<br />1" +
                " &lt; 3<br />26 &gt; 17<br />D:\\&quot;My Documents\\&quot;File<br />can&quot;t<br />";
        assertEquals(expectedString, converter.convert());
    }
}