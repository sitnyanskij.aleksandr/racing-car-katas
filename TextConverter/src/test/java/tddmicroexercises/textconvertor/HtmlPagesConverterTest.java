package tddmicroexercises.textconvertor;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HtmlPagesConverterTest {

    private HtmlPagesConverter converter;

    @Test
    @DisplayName("Method convert all lines in file into one html string")
    void convertToHtmlWithoutPageBreakTest() throws IOException {
        this.converter = new HtmlPagesConverter("src/test/resources/TextForHtmlTextConverter.txt");
        String expectedString = "some text &amp; text<br />1" +
                " &lt; 3<br />26 &gt; 17<br />D:\\&quot;My Documents\\&quot;File<br />can&quot;t<br />";
        assertEquals(expectedString, converter.convert(0));
    }

    @MethodSource("pages")
    @ParameterizedTest
    @DisplayName("Method convert chosen page into html string")
    void convertPagesToHtmlTest(int page, String htmlValue) throws IOException {
        this.converter = new HtmlPagesConverter("src/test/resources/TextForHtmlPageConverter.txt");
        assertEquals(htmlValue, converter.convert(page));
    }

    static Stream<Arguments> pages() {
        return Stream.of(
                Arguments.arguments(0, "some text &amp; text<br />1 &lt; 3<br />"),
                Arguments.arguments(1, "26 &gt; 17<br />D:\\&quot;My Documents\\&quot;File<br />can&quot;t<br />")
        );
    }
}