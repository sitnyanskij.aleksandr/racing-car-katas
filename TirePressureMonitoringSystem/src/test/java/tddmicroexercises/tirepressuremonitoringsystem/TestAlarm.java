package tddmicroexercises.tirepressuremonitoringsystem;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TestAlarm {

    private Alarm alarm;

    @BeforeEach
    void initializeServices() {
        this.alarm = new Alarm();
    }

    @Test
    @DisplayName("By default alarm is off")
    void isAlarmOffTest() {
        assertFalse(alarm.isAlarmOn());
    }

    @Test
    @DisplayName("Alarm on when pressure are lower then low threshold")
    void isAlarmOnByLowThresholdTest() {
        alarm.check(16.2);
        assertTrue(alarm.isAlarmOn());
    }

    @Test
    @DisplayName("Alarm on when pressure are higher then higher threshold")
    void isAlarmOnByHighThresholdTest() {
        alarm.check(29.2);
        assertTrue(alarm.isAlarmOn());
    }
}