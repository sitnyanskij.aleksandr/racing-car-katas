package tddmicroexercises.tirepressuremonitoringsystem;

public class Alarm {

    private static final double LOW_PRESSURE_THRESHOLD = 17;
    private static final double HIGH_PRESSURE_THRESHOLD = 21;
    private boolean alarmOn;

    public void check(double psiPressureValue) {
        if (checkLowPressureThreshold(psiPressureValue) || checkHighPressureThreshold(psiPressureValue)) {
            alarmOn = true;
        }
    }

    public boolean isAlarmOn() {
        return alarmOn;
    }

    private boolean checkLowPressureThreshold(double psiPressureValue) {
        return psiPressureValue < LOW_PRESSURE_THRESHOLD;
    }

    private boolean checkHighPressureThreshold(double psiPressureValue) {
        return HIGH_PRESSURE_THRESHOLD < psiPressureValue;
    }
}