package tddmicroexercises.tirepressuremonitoringsystem;

import static java.lang.System.*;
import static tddmicroexercises.tirepressuremonitoringsystem.Sensor.popNextPressurePsiValue;

public class App {

    public static void main(String[] args) {
        Alarm alarm = new Alarm();

        for (int times = 0; times < 10; times++) {
            alarm.check(popNextPressurePsiValue());
            out.println(alarm.isAlarmOn());
        }
    }
}