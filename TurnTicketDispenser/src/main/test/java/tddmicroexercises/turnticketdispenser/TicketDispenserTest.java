package tddmicroexercises.turnticketdispenser;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class TicketDispenserTest {

    private static final TicketDispenser firstDispenser = new TicketDispenser();
    private static final TicketDispenser secondDispenser = new TicketDispenser();

    @MethodSource("ticketNumbers")
    @ParameterizedTest
    @DisplayName("Numbers of given ticket from dispensers are unique")
    void ticketNumbersTest(int expectedNumber, int numberFromDispenser) {
        assertEquals(expectedNumber, numberFromDispenser);
    }

    static Stream<Arguments> ticketNumbers() {
        return Stream.of(
                Arguments.arguments(0, firstDispenser.getTurnTicket().getTurnNumber()),
                Arguments.arguments(1, secondDispenser.getTurnTicket().getTurnNumber()),
                Arguments.arguments(2, firstDispenser.getTurnTicket().getTurnNumber()),
                Arguments.arguments(3, secondDispenser.getTurnTicket().getTurnNumber()),
                Arguments.arguments(4, firstDispenser.getTurnTicket().getTurnNumber()),
                Arguments.arguments(5, secondDispenser.getTurnTicket().getTurnNumber()),
                Arguments.arguments(6, firstDispenser.getTurnTicket().getTurnNumber()),
                Arguments.arguments(7, secondDispenser.getTurnTicket().getTurnNumber()),
                Arguments.arguments(8, firstDispenser.getTurnTicket().getTurnNumber())
        );
    }
}