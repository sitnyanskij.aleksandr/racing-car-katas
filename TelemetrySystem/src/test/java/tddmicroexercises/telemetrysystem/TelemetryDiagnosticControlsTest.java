package tddmicroexercises.telemetrysystem;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import tddmicroexercises.telemetrysystem.entites.TelemetryClient;
import tddmicroexercises.telemetrysystem.exceptions.UnableConnectException;
import tddmicroexercises.telemetrysystem.system.TelemetryDiagnosticControls;

import static org.junit.jupiter.api.Assertions.*;

class TelemetryDiagnosticControlsTest {

    private TelemetryDiagnosticControls telemetryDiagnosticControls;
    private TelemetryClient telemetryClient;

    @BeforeEach
    void initializeServices() {
        this.telemetryDiagnosticControls = new TelemetryDiagnosticControls();
        this.telemetryClient = new TelemetryClient();
    }

    @Test
    @DisplayName("CheckTransmission should send a diagnostic message and receive a status message response")
    void CheckTransmissionTest() throws UnableConnectException {
        assertEquals(getExpectedResponse(), telemetryDiagnosticControls.checkTransmission(telemetryClient));
    }

    @Test
    @DisplayName("Test successful if client can reconnect otherwise test fail")
    void tryReconnectTest() {
        try {
            telemetryClient.disconnect();
            telemetryDiagnosticControls.tryReconnect(telemetryClient);
        } catch (UnableConnectException e) {
            fail();
        }
    }

    private String getExpectedResponse() {
        return "LAST TX rate................ 100 MBPS\r\n" + "HIGHEST TX rate............. 100 MBPS\r\n"
                + "LAST RX rate................ 100 MBPS\r\n" + "HIGHEST RX rate............. 100 MBPS\r\n"
                + "BIT RATE.................... 100000000\r\n" + "WORD LEN.................... 16\r\n"
                + "WORD/FRAME.................. 511\r\n" + "BITS/FRAME.................. 8192\r\n"
                + "MODULATION TYPE............. PCM/FM\r\n" + "TX Digital Los.............. 0.75\r\n"
                + "RX Digital Los.............. 0.10\r\n" + "BEP Test.................... -5\r\n"
                + "Local Rtrn Count............ 00\r\n" + "Remote Rtrn Count........... 00";
    }
}