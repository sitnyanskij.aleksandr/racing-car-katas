package tddmicroexercises.telemetrysystem.system;

import tddmicroexercises.telemetrysystem.entites.TelemetryClient;
import tddmicroexercises.telemetrysystem.exceptions.UnableConnectException;

public class TelemetryDiagnosticControls {

    private static final String DIAGNOSTIC_CHANNEL_CONNECTION_STRING = "*111#";

    private final StringBuilder diagnosticInfo = new StringBuilder();

    public String getDiagnosticInfo() {
        return String.valueOf(diagnosticInfo);
    }

    public void setDiagnosticInfo(String diagnosticInfo) {
        this.diagnosticInfo.append(diagnosticInfo);
    }

    public String checkTransmission(TelemetryClient telemetryClient) {
        telemetryClient.send(TelemetryClient.DIAGNOSTIC_MESSAGE);
        return telemetryClient.receive();
    }

    public void tryReconnect(TelemetryClient telemetryClient) throws UnableConnectException {
        clearDiagnosticInfo();
        boolean isConnect = telemetryClient.getOnlineStatus();
        int retryLeft = 3;
        while (!isConnect && retryLeft > 0) {
            telemetryClient.connect(DIAGNOSTIC_CHANNEL_CONNECTION_STRING);
            isConnect = telemetryClient.getOnlineStatus();
            retryLeft -= 1;
        }
        if (!isConnect) {
            throw new UnableConnectException("Unable to connect.");
        }
        setDiagnosticInfo(checkTransmission(telemetryClient));
    }

    private void clearDiagnosticInfo() {
        diagnosticInfo.delete(0, diagnosticInfo.length());
    }
}