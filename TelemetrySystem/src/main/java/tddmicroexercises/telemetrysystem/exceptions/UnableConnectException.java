package tddmicroexercises.telemetrysystem.exceptions;

public class UnableConnectException extends Exception {

    public UnableConnectException(String message) {
        super(message);
    }
}
